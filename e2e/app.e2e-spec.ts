import { RandomTextGeneratorPage } from './app.po';

describe('random-text-generator App', function() {
  let page: RandomTextGeneratorPage;

  beforeEach(() => {
    page = new RandomTextGeneratorPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
