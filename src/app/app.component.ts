import { Component, OnInit } from '@angular/core';

import { Clause } from './clause/clause.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'app works!';
  clause: Clause = new Clause();
  
  ngOnInit(){
    this.clause.nodes.push({text: "Test", position: 1, linkId: null});
  }
}
