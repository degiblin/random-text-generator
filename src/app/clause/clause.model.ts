import { Node } from './node.model';

export class Clause {
    weight: number;
    nodes: Node[];
    constructor(){
        this.weight = 1;
        this.nodes = [];
    }
}
