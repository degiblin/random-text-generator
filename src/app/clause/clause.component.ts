import { Component, OnInit, Input } from '@angular/core';

import { Clause } from './clause.model';
import { Node } from './node.model';
import { RtgDataService } from '../rtg-data.service';


@Component({
  selector: 'rtg-clause',
  templateUrl: './clause.component.html',
  styleUrls: ['./clause.component.css']
})
export class ClauseComponent implements OnInit {

  @Input() clause: Clause;
  newNode: Node;


  constructor(private rtgDataService: RtgDataService) {
    this.newNode = new Node();
  }

  ngOnInit() {
    this.clause.nodes.sort((a, b) => a.position - b.position);
  }

  formatted(node: Node) {
    let result = "";
    result += node.text
    if (node.linkId != null) {
      result += ' ' + this.rtgDataService.getListName(node.linkId);
    }

    return result;
  }

  checkIndex(last: boolean) {
    if (last) {
      console.log(last);
      if (this.newNode.text || this.newNode.linkId) {
        return " ";
      } else {
        return "";
      }
    }
    return " ";
  }

  onAdd() {
    this.clause.nodes.push(this.newNode);
    this.newNode = new Node();
  }
}
