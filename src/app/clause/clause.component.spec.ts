/* tslint:disable:no-unused-variable */
import { async, fakeAsync, tick, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { Clause } from './clause.model';
import { Node } from './node.model';

import { ClauseComponent } from './clause.component';
import { RtgDataService } from '../rtg-data.service';

describe('ClauseComponent', () => {
  let component: ClauseComponent;
  let fixture: ComponentFixture<ClauseComponent>;
  let page: Page;
  let rtgDataService: RtgDataService;

  class Page {
    combinedText: HTMLElement;
    textInput: HTMLInputElement;
    addButton: HTMLButtonElement;

    constructor() {
    }

    /** Add page clauses after hero arrives */
    addPageElements() {
      if (component.newNode) {
        // have a hero so these clauses are now in the DOM
        this.combinedText = fixture.debugElement.query(By.css('.well.well-sm')).nativeElement;
        this.textInput = fixture.debugElement.query(By.css('.form-inline input[type="text"]')).nativeElement;
        this.addButton = fixture.debugElement.query(By.css('.btn.btn-primary')).nativeElement;
      }
    }
  }
  function createComponent() {

    let RtgDataServiceStub = {
      getListName: (id: number) => "Test List " + id
    }

    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ClauseComponent],
      providers: [{ provide: RtgDataService, useValue: RtgDataServiceStub }]
    })
      .compileComponents();

    fixture = TestBed.createComponent(ClauseComponent);
    component = fixture.componentInstance;
    page = new Page();
    rtgDataService = fixture.debugElement.injector.get(RtgDataService);

    component.clause = {
      weight: 1,
      nodes: [{ text: 'Node 1', position: 0, linkId: 1 }, { text: 'Node 3', position: 2, linkId: null }, { text: 'Node 2', position: 1, linkId: null }]
    }
    // 1st change detection triggers ngOnInit
    fixture.detectChanges();
    return fixture.whenStable().then(() => {
      // 2nd change detection displays the async
      fixture.detectChanges();
      page.addPageElements();
    });
  }



  beforeEach(async(() => {
    createComponent();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should contain an Clause with 3 Nodes', () => {
    expect(component.clause.nodes.length).toBe(3);
  });

  it('should contain an Clause with weight 1', () => {
    expect(component.clause.weight).toBe(1);
  });

  it('should contain a newNode', () => {
    expect(component.newNode).toBeTruthy();
  });

  it('should render 1 node forms', () => {
    let el = fixture.debugElement.queryAll(By.css('.form-inline'));
    expect(el.length).toBe(1);
  });

  it('should update newNode on changes', fakeAsync(() => {
    fixture.detectChanges();
    page.textInput.value = 'test';
    page.textInput.dispatchEvent(new Event('input'));

    tick();
    fixture.detectChanges();

    expect(component.newNode.text).toEqual('test');
  }));

  it('should format nodes', () => {
    let result = component.formatted({text: 'Test', position:1, linkId: 1});
    expect(result).toBe('Test Test List 1');
  });

  it('should display completed nodes combined in a well', fakeAsync(() => {
    fixture.detectChanges();
    let sortedNodes = component.clause.nodes.sort((a, b) => a.position - b.position);
    expect(sortedNodes.length).toBeGreaterThan(0);
    let expectedText = "";
    sortedNodes.forEach((node, i) => {
      if (i !== 0) {
        expectedText += ' ';
      }
      expectedText += node.text
      if (node.linkId != null) {
        expectedText += ' ' + rtgDataService.getListName(node.linkId);
      }
    });
    expect(page.combinedText.textContent).toContain(expectedText);
  }));

  it('should add newNode to clause and clear on button click', fakeAsync(() => {
    let expectedText = "New Node";
    let currentCount = component.clause.nodes.length;
    page.textInput.value = expectedText;
    page.textInput.dispatchEvent(new Event('input'));

    tick();
    fixture.detectChanges();
    
    expect(page.combinedText.textContent).toContain(expectedText);

    page.addButton.dispatchEvent(new Event('click'));

    tick();
    fixture.detectChanges();

    expect(component.clause.nodes.length).toEqual(currentCount + 1);
    expect(page.textInput.textContent).toEqual("");
  }));

});
