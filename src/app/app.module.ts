import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ClauseComponent } from './clause/clause.component';
import { RtgDataService } from './rtg-data.service';

@NgModule({
  declarations: [
    AppComponent,
    ClauseComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [RtgDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
