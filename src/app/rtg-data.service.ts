import { Injectable } from '@angular/core';
import { Clause } from './clause/clause.model';

@Injectable()
export class RtgDataService {

  constructor() { }

  getListName(id: number) { }
  createList(name: string){}
  addClause(listName: string, clause: Clause){}
  updateClause(listName: string, clause: Clause){}
}
