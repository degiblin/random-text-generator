/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RtgDataService } from './rtg-data.service';

describe('RtgDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RtgDataService]
    });
  });

  it('should ...', inject([RtgDataService], (service: RtgDataService) => {
    expect(service).toBeTruthy();
  }));
});
