var gen_data = {};
gen_data['sequence'] = [
    'The {type}'
]

gen_data['type'] = {
    '1-3': '{colony}',
    '4-6': 'Wreckage of the {ship} on {planet}',
    '7-9': '{ruins}{city} on {planet}',
    '10': '{planet} Space Station'
}

gen_data['colony'] = [
    '{planet} Colony'
]

gen_data['ship'] = [
    'USS Enterprise',
    'USS Voyager'
]

gen_data['planet'] = [
    'Mercury',
    'Venus',
    'Earth',
    'Mars',
    'Jupiter',
    'Saturn',
    'Uranus',
    'Neptune',
    'Pluto'
]

gen_data['ruins'] = {
    '1-8': '',
    '9-10': 'Ruins of '
}

gen_data['city'] = {
    '1-6': '{cityname} City',
    '8-9': 'City of {cityname}',
    '10': '{cityname}polis'
}

gen_data['cityname'] = [
    'Indian',
    'Anna',
    'Charles',
    'Johnson'
]
module.exports = gen_data;