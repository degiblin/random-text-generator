var gen_data = {};
gen_data['sequence'] = [
	'A {npc}, from a {location}, needs someone to {job}.'
];
gen_data['npc'] = [
	'{gender} {race} {occupation}'
]
gen_data['gender'] = [
	'male', 'female'
];
gen_data['race'] = [
	'canine',
	'feline',
	'racoon',
	'rodent',
	'bovine',
	'sheep'
];
gen_data['occupation'] = [
	'mayor',
	'shop owner',
	'farmer',
	'warrior',
	'doctor',
	'artist'
];
gen_data['location'] = [
	'{size} town',
	'{size} city',
	'{size} village'
];
gen_data['size'] = [
	'small',
	'medium',
	'large'
];
gen_data['job'] = [
	'{type} a {objective}'
];
gen_data['type'] = [
	'transport',
	'retrieve',
	'locate'
];
gen_data['objective'] = [
	'medicine',
	'jewel',
	'person'
];

module.exports = gen_data;